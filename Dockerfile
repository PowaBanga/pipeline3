FROM python:3.10.1-bullseye
WORKDIR /home/py
COPY . .
RUN pip3 install -r requirements.txt
EXPOSE 8080
ENTRYPOINT [ "python3" ]
CMD [ "app.py" ]
